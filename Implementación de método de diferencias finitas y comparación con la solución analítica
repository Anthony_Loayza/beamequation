##############################################################################
#################### Ecuacion de la viga #####################################
##############################################################################

import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg


##############################################################################


h = 0.1                             # discretizacion en la longitud (x)
k = 0.001                           # discretizacion del tiempo
lam = ((k**2))/(h**4)

t_aprox = 0.01                      # tiempo al que se quiere aproximar la solucion
n_iteraciones = (t_aprox/k)         # numero de interaciones
    

discre_x = h
n = int((1/discre_x)-1)
x = []                              # generar valores de discretizacion en x       
for i in range(1,n+1):              # pero sin los extremos
    x.append(round(discre_x*i,2))
xs = x


##############################################################################
################## Generar matriz pentadigonal ###############################

diagonal = [1, -4, 6, -4, 1, 0]
filas = np.zeros(n-2)
filas[0]=1
columnas = np.zeros(n)
for i in range(len(diagonal)):
    columnas[i]=diagonal[i]

fila1 = np.zeros(n)
fila1[0] = 5
fila1[1] = -4
fila1[2] = 1
fila2 = np.zeros(n)
fila2[0] = -4
fila2[1] = 6
fila2[2] = -4
fila2[3] = 1

Matriz = np.zeros((n,n))

diagonal_desde1 = scipy.linalg.toeplitz(filas, columnas)

for i in range(n):
    Matriz[0,:]=fila1
    Matriz[1,:]=fila2
    Matriz[i,:]=diagonal_desde1[i-2,:]
    
Matriz[-1,-1] = 5

A = Matriz
##############################################################################
##############################################################################

g_0 = np.zeros(n)                   # condicion incial

u_0 = np.zeros(n) 

for i in range(n):                  # evaluar en la segunda condicion inicial
    g_0[i] = np.sin(np.pi*xs[i])

b = np.zeros(n)                     # vector b

u_1 = u_0 + g_0*k
u_2 = 2*u_1-u_0+lam*b+lam*A@u_0

for t in range(int(n_iteraciones-2)):
    u_n = 2*u_2-u_1+lam*b+lam*A@u_1
    u_1 = u_2
    u_2 = u_n

# Agregar los valores extremos x
xss = [0]
for i in range(n):
    xss.append(xs[i])
xss.append(1)

# Agregrar las condiciones de contorno de u(x,t)
u = [0]
for i in u_n:
    u.append(i)
u.append(0)

##############################################################################
###################### solucion analitica ####################################
xsl = np.linspace(0,1,50)

def sol_analitica(t,x):
    return (np.sin((np.pi**2)*t)*np.sin(np.pi*x))/(np.pi**2)

##############################################################################
########################## Graficar ##########################################
plt.plot(xsl,sol_analitica(t_aprox,xsl),color = 'black', label = 'Solución analítica')
plt.plot(xss,u, 'o-', color = 'orange', label = 'Aproximación')
plt.title('$t=$'+str(t_aprox))
plt.xlabel('$x$')
plt.ylabel('$u(t,x)$')
plt.legend()
plt.grid()